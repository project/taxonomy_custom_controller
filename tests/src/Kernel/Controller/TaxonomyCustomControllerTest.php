<?php

namespace Drupal\Tests\taxonomy_custom_controller\Kernel\Controller;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\taxonomy_custom_controller\Controller\TaxonomyCustomController;
use Drupal\taxonomy_custom_controller\Event\TermPageBuildEvent;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\views\Entity\View;
use Prophecy\Argument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides test for controller that handles overrides.
 *
 * @coversDefaultClass \Drupal\taxonomy_custom_controller\Controller\TaxonomyCustomController
 * @group taxonomy_custom_controller
 */
final class TaxonomyCustomControllerTest extends KernelTestBase {

  use ProphecyTrait;
  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'taxonomy_custom_controller',
    'taxonomy',
    'views',
    'text',
    'user',
    'filter',
    'system',
    'node',
  ];

  /**
   * The class resolver.
   */
  protected ClassResolverInterface $classResolver;

  /**
   * Tests that event is called and processed as expected.
   */
  public function testEvent(): void {
    $passed_event = NULL;

    $event_dispatcher = $this->prophesize(EventDispatcherInterface::class);
    $event_dispatcher
      ->dispatch(Argument::cetera())
      ->will(static function (array $args) use (&$passed_event) {
        $passed_event = $args[0];

        return $args[0];
      });
    $this->container->set('event_dispatcher', $event_dispatcher->reveal());

    $vocabulary = $this->createVocabulary();
    $term = $this->createTerm($vocabulary);

    $controller = $this
      ->classResolver
      ->getInstanceFromDefinition(TaxonomyCustomController::class);
    \assert($controller instanceof TaxonomyCustomController);
    $controller->build($term);

    self::assertInstanceOf(TermPageBuildEvent::class, $passed_event);
    self::assertEquals($term->id(), $passed_event->getTaxonomyTerm()->id());
    self::assertEquals(
      $term->bundle(),
      $passed_event->getTaxonomyTerm()->bundle(),
    );
  }

  /**
   * Tests that provided build array from event is used as controller result.
   */
  public function testProvideBuildArray(): void {
    $expected_result = ['#markup' => 'Hello!'];

    $event_dispatcher = $this->prophesize(EventDispatcherInterface::class);
    $event_dispatcher
      ->dispatch(Argument::cetera())
      ->will(static function (array $args) use (&$passed_event, $expected_result) {
        $passed_event = $args[0];
        \assert($passed_event instanceof TermPageBuildEvent);
        $passed_event->setBuildArray($expected_result);

        return $args[0];
      });
    $this->container->set('event_dispatcher', $event_dispatcher->reveal());

    $vocabulary = $this->createVocabulary();
    $term = $this->createTerm($vocabulary);

    $controller = $this
      ->classResolver
      ->getInstanceFromDefinition(TaxonomyCustomController::class);
    \assert($controller instanceof TaxonomyCustomController);
    $result = $controller->build($term);

    self::assertEquals($expected_result, $result);
  }

  /**
   * Tests fallback into default views.
   *
   * When subscribers doesn't provide a result, it should fall back into default
   * Views.
   */
  public function testViewsFallback(): void {
    $vocabulary = $this->createVocabulary();
    $term = $this->createTerm($vocabulary);

    $view = View::create([
      'id' => 'taxonomy_term',
    ]);
    $view->addDisplay('page', NULL, 'page_1');
    $view->save();

    $controller = $this
      ->classResolver
      ->getInstanceFromDefinition(TaxonomyCustomController::class);
    \assert($controller instanceof TaxonomyCustomController);
    $result = $controller->build($term);

    $expected = views_embed_view('taxonomy_term', 'page_1', $term->id());

    self::assertEquals($expected, $result);
  }

  /**
   * Tests that controller fallback intro term display if everything is missed.
   *
   * This should cover cases when no event subscriber added anything into
   * result
   * and when default Views is disabled/removed from the system.
   *
   * @see https://www.drupal.org/project/taxonomy_custom_controller/issues/3353441
   */
  public function testTermFallback(): void {
    $vocabulary = $this->createVocabulary();
    $term = $this->createTerm($vocabulary);

    $controller = $this
      ->classResolver
      ->getInstanceFromDefinition(TaxonomyCustomController::class);
    \assert($controller instanceof TaxonomyCustomController);
    $result = $controller->build($term);

    $view_builder = $this
      ->container
      ->get('entity_type.manager')
      ->getViewBuilder('taxonomy_term');
    \assert($view_builder instanceof EntityViewBuilderInterface);

    $expected = $view_builder->view($term);

    self::assertSame($expected, $result);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('view');
    $this->installEntitySchema('user');
    $this->installSchema('user', 'users_data');
    $this->installEntitySchema('taxonomy_vocabulary');
    $this->installEntitySchema('taxonomy_term');
    $this->classResolver = $this->container->get('class_resolver');
  }

}
