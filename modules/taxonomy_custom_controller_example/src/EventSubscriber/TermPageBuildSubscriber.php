<?php

namespace Drupal\taxonomy_custom_controller_example\EventSubscriber;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\taxonomy_custom_controller\Event\TaxonomyCustomControllerEvents;
use Drupal\taxonomy_custom_controller\Event\TermPageBuildEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides subscriber for term page build.
 */
class TermPageBuildSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      TaxonomyCustomControllerEvents::PAGE_BUILD => 'onTermPageBuild',
    ];
  }

  /**
   * Reacts on term page build.
   *
   * @param \Drupal\taxonomy_custom_controller\Event\TermPageBuildEvent $event
   *   The event.
   */
  public function onTermPageBuild(TermPageBuildEvent $event) {
    $taxonomy_term = $event->getTaxonomyTerm();

    $build = [];
    // Provide default content to react on all pages.
    $build['hello'] = [
      '#markup' => new TranslatableMarkup('This content was added from @module just as an example.', [
        '@module' => 'taxonomy_custom_controller_example',
      ]),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];

    if (!$taxonomy_term->get('description')->isEmpty()) {
      // Adds term description on top of the default views.
      $build['description'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['wrapper-for-custom-added-description'],
        ],
        'description' => $taxonomy_term->get('description')->view(),
      ];
    }

    $view = views_embed_view('taxonomy_term', 'page_1', $taxonomy_term->id());
    if ($view) {
      $build['view'] = $view;
    }

    $event->setBuildArray($build);
  }

}
