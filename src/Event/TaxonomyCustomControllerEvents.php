<?php

namespace Drupal\taxonomy_custom_controller\Event;

/**
 * Provides list of event names.
 */
abstract class TaxonomyCustomControllerEvents {

  /**
   * Event which called for building render array for term controller.
   *
   * @Event
   *
   * @see \Drupal\taxonomy_custom_controller\Event\TermPageBuildEvent
   * @see \Drupal\taxonomy_custom_controller\Controller\TaxonomyCustomController
   */
  const PAGE_BUILD = 'taxonomy_custom_controller.term_page_build';

}
