<?php

namespace Drupal\taxonomy_custom_controller\Event;

use Drupal\taxonomy\TermInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides event which fires when prepare term page render array result.
 */
class TermPageBuildEvent extends Event {

  /**
   * The render array for page.
   *
   * @var array
   */
  protected $build = [];

  /**
   * The taxonomy term.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  protected $taxonomyTerm;

  /**
   * Constructs a new TermPageBuildEvent object.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   *   The taxonomy term.
   */
  public function __construct(TermInterface $taxonomy_term) {
    $this->taxonomyTerm = $taxonomy_term;
  }

  /**
   * Sets build array as result for page.
   *
   * @param array $build
   *   The render array.
   */
  public function setBuildArray(array $build): void {
    $this->build = $build;
  }

  /**
   * Gets build result for the page.
   *
   * @return array
   *   The render array.
   */
  public function getBuildArray(): array {
    return $this->build;
  }

  /**
   * Gets taxonomy term entity.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The taxonomy term.
   */
  public function getTaxonomyTerm(): TermInterface {
    return $this->taxonomyTerm;
  }

}
