<?php

namespace Drupal\taxonomy_custom_controller\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy_custom_controller\Event\TaxonomyCustomControllerEvents;
use Drupal\taxonomy_custom_controller\Event\TermPageBuildEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides custom controller for taxonomy routes.
 */
final class TaxonomyCustomController implements ContainerInjectionInterface {

  /**
   * The event dispatcher.
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new self();
    $instance->eventDispatcher = $container->get('event_dispatcher');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * The build process for term pages.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   *   The taxonomy term entity.
   *
   * @return array|null
   *   The result render array.
   */
  public function build(TermInterface $taxonomy_term) {
    $event = new TermPageBuildEvent($taxonomy_term);
    $this->eventDispatcher->dispatch($event, TaxonomyCustomControllerEvents::PAGE_BUILD);

    $build = $event->getBuildArray();
    if (!empty($build)) {
      return $build;
    }

    $view = views_embed_view('taxonomy_term', 'page_1', $taxonomy_term->id());
    if ($view) {
      return $view;
    }

    // If custom result is not provided and views is disabled or removed, just
    // render term.
    $view_builder = $this->entityTypeManager->getViewBuilder('taxonomy_term');

    return $view_builder->view($taxonomy_term);
  }

}
