<?php

namespace Drupal\taxonomy_custom_controller\EventSubscriber;

use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides subscribers for routes.
 */
class RouteAlterSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      RoutingEvents::ALTER => ['onAlterRoutes', -200],
    ];
  }

  /**
   * Reacts on route alter.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route build event.
   */
  public function onAlterRoutes(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();
    if ($route = $collection->get('entity.taxonomy_term.canonical')) {
      $route->setDefault('_controller', 'Drupal\taxonomy_custom_controller\Controller\TaxonomyCustomController::build');
    }
  }

}
