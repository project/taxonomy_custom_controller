CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Taxonomy Custom Controller module is a default Taxonomy Term entity
canonical controller override with event.

It will help you to subscribe to event and change content of term page.
You can change it for specific vocabularies, or specific term with
conditions you want!

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

* Install the Taxonomy Custom Controller module as you would normally install
  a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.

USAGE
-------------

Simple example:

```php
/**
 * Provides subscriber for term page build.
 */
class TermPageBuildSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      TaxonomyCustomControllerEvents::PAGE_BUILD => 'onTermPageBuild',
    ];
  }

  /**
   * Reacts on term page build.
   *
   * @param \Drupal\taxonomy_custom_controller\Event\TermPageBuildEvent $event
   *   The event.
   */
  public function onTermPageBuild(TermPageBuildEvent $event) {
    $taxonomy_term = $event->getTaxonomyTerm();
    if ($taxonomy_term->bundle() != 'tags') {
      return;
    }

    // Adds term description on top of the default views.
    $build = [];
    if (!$taxonomy_term->get('description')->isEmpty()) {
      $build['description'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['wrapper-for-custom-added-description'],
        ],
        'description' => $taxonomy_term->get('description')->view(),
      ];
    }
    $build['view'] = views_embed_view(
      'taxonomy_term',
      'page_1',
      $taxonomy_term->id(),
    );
    $event->setBuildArray($build);
  }

}
```

See **taxonomy_custom_controller_example** module for full example.

MAINTAINERS
-----------

 * Nikita Malyshev (Niklan) - https://www.drupal.org/u/niklan
